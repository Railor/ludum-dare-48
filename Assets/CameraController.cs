using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float cameraSpeed = 10;
    public BattleManager battleManager;
    private Camera _camera;
    private float minX;
    private float maxX;
    private float minY = -20.5f;
    private float maxY = 40;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

// Start is called before the first frame update
    void Start()
    {
        minX = -(battleManager.fieldWidth - _camera.orthographicSize) - 10;
        maxX = battleManager.fieldWidth - _camera.orthographicSize - 1 + 30;
    }


    // Update is called once per frame
    void Update()
    {
//        Vector3 newTrans = transform.position + (Time.deltaTime * cameraSpeed * new Vector3(Input.GetAxis("Horizontal"), 0, 0));
//        if (newTrans.x <= minX) {
//            newTrans.x = minX;
//        }
//
//        if (newTrans.x >= maxX)
//        {
//            newTrans.x = maxX;
//        }
//        
//        if (newTrans.y <= minY)
//        {
//            newTrans.y = minY;
//        }
//
//        if (newTrans.y >= maxY)
//        {
//            newTrans.y = maxY;
//        }
//
//        transform.position = newTrans;
    }
}