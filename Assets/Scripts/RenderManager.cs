using System;
using TMPro;
using UnityEngine;

public class RenderManager : MonoBehaviour
{
    public CharacterRender[] playerCharacters;
    public CharacterRender playerRenderPrefab;
    public CharacterRender trollRenderPrefab;
    public CharacterRender ogreRenderPrefab;
    public CharacterRender knightRenderPrefab;
    public Transform characterContainer;

    public Transform plinkoContainer;
    public Transform catcherContainer;
    public GameObject plinkoPrefab;

    private bool catcherDirection;
    public float catcherSpeed = 5f;
    private float fieldSize;
    public static RenderManager instance;
    public AbilityUI draggedAbility;

    public TextMeshProUGUI depthText;
    private float minX;
    private float maxX;
    private float minY = -20.5f;
    private float maxY = 40;

    private void Awake()
    {
        instance = this;
        GameEvents.instance.WAVE_STARTED += GameEventsOnWaveStarted;
    }

    private void GameEventsOnWaveStarted(int obj)
    {
        depthText.text = "Depth: " + obj;
    }

    private void Start()
    {
        fieldSize = BattleManager.instance.fieldWidth + 10;
        for (int x = 0; x < 5; x++)
        {
            for (int i = 0; i <= fieldSize / 2; i++)
            {
                GameObject go = Instantiate(plinkoPrefab, plinkoContainer);
                go.transform.position = new Vector3(-fieldSize - 5 + i * 4 + (x % 2 == 0 ? 2 : 0), 23 - (x * 4f), 0);
            }
        }

        minX = -BattleManager.instance.fieldWidth * 2;
        maxX = BattleManager.instance.fieldWidth * 2;
    }

    public void Update()
    {
        Vector3 newTrans = catcherContainer.position + (Time.deltaTime * catcherSpeed * new Vector3(Input.GetAxis("Horizontal"), 0, 0));
        if (newTrans.x <= minX)
        {
            newTrans.x = minX;
        }

        if (newTrans.x >= maxX)
        {
            newTrans.x = maxX;
        }

        if (newTrans.y <= minY)
        {
            newTrans.y = minY;
        }

        if (newTrans.y >= maxY)
        {
            newTrans.y = maxY;
        }

        catcherContainer.position = newTrans;
//
//        float xSpeed = catcherSpeed * Time.deltaTime * (catcherDirection ? 1 : -1);
//        Vector3 targetPos = new Vector3(catcherContainer.position.x + xSpeed, catcherContainer.position.y, catcherContainer.position.z);
//        if (targetPos.x <= -fieldSize)
//        {
//            catcherDirection = !catcherDirection;
//            targetPos.x += 1 * Time.deltaTime;
//        }
//        else if (targetPos.x >= fieldSize / 3f)
//        {
//            catcherDirection = !catcherDirection;
//            targetPos.x -= 1 * Time.deltaTime;
//        }

//        catcherContainer.position = targetPos;
    }
}