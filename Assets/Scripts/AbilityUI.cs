using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class AbilityUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IPointerDownHandler, IDragHandler, IEndDragHandler
{
    public CharacterAbility characterAbility;
    public bool isDraggable = false;
    public int cost;
    public TextMeshProUGUI text;
    [HideInInspector] public CharacterBuyPanel characterBuyPanel;

    public void OnPointerEnter(PointerEventData eventData)
    {
        string content = characterAbility.getDescription();
        if (cost == 0)
        {
            content += "\n\n Left Click Upgrade: " + characterAbility.getUpgradeCost();
            content += "\n Right click Sell: " + characterAbility.getUpgradeCost();
        }
        else
        {
            content += "\n\n Drag onto Hero to buy";
        }

        ToolTipDisplay.setText(characterAbility.getTitle(), content);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ToolTipDisplay.hide();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (isDraggable && BattleManager.instance.hasGold(cost))
        {
            CharacterBuyPanel.draggedAbility = this;
            RenderManager.instance.draggedAbility.characterAbility = characterAbility;
            RenderManager.instance.draggedAbility.cost = cost;
            RenderManager.instance.draggedAbility.text.text = characterAbility.ability.name;
            RenderManager.instance.draggedAbility.gameObject.SetActive(true);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //Left
        if (eventData.pointerId == -1)
        {
            if (characterBuyPanel)
            {
                characterBuyPanel.upgradeAbility(this);
            }
        }
        else
        {
            if (characterBuyPanel)
            {
                characterBuyPanel.sellAbility(this);
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isDraggable && BattleManager.instance.hasGold(cost))
        {
            RenderManager.instance.draggedAbility.gameObject.SetActive(true);
            RenderManager.instance.draggedAbility.transform.position = Input.mousePosition + Vector3.up * 5;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        RenderManager.instance.draggedAbility.gameObject.SetActive(false);
    }
}