using System.Collections.Generic;
using UnityEngine;

public class Character
{
    public CharacterRender render;
    public CharacterStats characterStats;
    public bool playerOwned;
    private BattleManager battleManager;

    private float windUpTime = -1;
    public List<Status> statuses = new List<Status>();
    public bool isDead = false;
    public LinkedListNode<Character> myNodePosition;

    public List<CharacterAbility> abilities = new List<CharacterAbility>();
    private CharacterAbility castedAbility;
    private CharacterAbility meleeAbility;

    public Character(bool playerOwned, BattleManager battleManager, int playerCharacterId = 0)
    {
        this.battleManager = battleManager;
        this.playerOwned = playerOwned;
        meleeAbility = new CharacterAbility(AbilityManager.meleeAbility, 1);

        createRender(playerCharacterId);
    }

    public void createRender(int playerCharacterId)
    {
        if (playerOwned)
        {
            render = GameObject.Instantiate(battleManager.renderManager.playerCharacters[playerCharacterId],
                battleManager.renderManager.characterContainer);
        }
        else
        {
            if (BattleManager.instance.currentWave % 10 == 0)
            {
                render = GameObject.Instantiate(battleManager.renderManager.ogreRenderPrefab, battleManager.renderManager.characterContainer);
            }
            else
            {
                render = GameObject.Instantiate(battleManager.renderManager.trollRenderPrefab, battleManager.renderManager.characterContainer);
            }
        }

        render.transform.localRotation = Quaternion.Euler(0, playerOwned ? 0 : 180, 0);
    }

    public void update(float dt)
    {
    }

    public float getPosition()
    {
        return render.transform.position.x;
    }

    public void tick(float dt)
    {
        characterStats.turnDelay -= dt;
        if (windUpTime >= 0)
        {
            windUpTime -= dt;

            if (windUpTime <= 0)
            {
                if (castedAbility != null)
                {
                    castedAbility.doAction(dt, this, battleManager);
                    castedAbility = null;
                }
            }
        }

        Character character = getInfrontCharacter(playerOwned);

        // TODO IF AT THE END, ATTACK THE BOSS AND WIN
        if (character == null || Mathf.Abs(getPosition() - character.getPosition()) >= 2f)
        {
            if (battleManager.moveForward(this))
            {
            }
        }

        for (int i = 0; i < abilities.Count; i++)
        {
            abilities[i].cooldown -= dt;
        }

        meleeAbility.cooldown -= dt;

        if (characterStats.turnDelay <= 0)
        {
            doTurn();
        }
    }

    private void doTurn()
    {
        doStatus();

        if (isDead)
        {
            return;
        }

        if (windUpTime <= 0)
        {
            bool usedAbility = false;
            for (int i = 0; i < abilities.Count; i++)
            {
                if (abilities[i].canUse(Time.fixedDeltaTime, this, battleManager))
                {
                    usedAbility = true;
                    castedAbility = abilities[i];
                    windUpTime = .7f;
                    render.attack();
                    characterStats.turnDelay += 1f;
                    break;
                }
            }

            if (!usedAbility)
            {
                if (meleeAbility.canUse(Time.fixedDeltaTime, this, battleManager))
                {
                    castedAbility = meleeAbility;
                    windUpTime = .7f;
                    render.attack();
                    characterStats.turnDelay += 1f;
                }
            }
        }
    }

    private void doStatus()
    {
        for (int i = statuses.Count - 1; i >= 0; i--)
        {
            statuses[i].doTurn(Time.fixedDeltaTime, this, battleManager);

            statuses[i].turnsRemaining--;

            if (statuses[i].turnsRemaining <= 0)
            {
                removeStatus(statuses[i]);
            }

            if (isDead)
            {
                return;
            }
        }
    }

    public void addStatus(Status status)
    {
        statuses.Add(status);
    }

    public void removeStatus(Status status)
    {
        statuses.Remove(status);
    }

    public float getForwardPosition()
    {
        return getPosition() + (playerOwned ? -.5f : .5f);
    }

    public void destroy()
    {
        if (isDead)
        {
            Debug.LogError("Already dead!");
            return;
        }

        ragDoll();
//        GameObject.Destroy(render.gameObject);
        isDead = true;
    }

    private void ragDoll()
    {
        render.ragdoll();
    }

    public Character getInfrontCharacter(bool playerTeam)
    {
        Character closestCharacter;
        if (playerTeam)
        {
            closestCharacter = myNodePosition.Previous?.Value;
        }
        else
        {
            closestCharacter = myNodePosition.Next?.Value;
        }

        return closestCharacter;
    }
}