using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class BattleManager : MonoBehaviour
{
    public RenderManager renderManager;
    public float groundHeight = 26.3f;
    public float fieldWidth = 100;
    private int maxSlot;
    private float slotOffset;
    private const float separationAmount = 3f;
    private LinkedList<Character> characters = new LinkedList<Character>();
    private bool lastSpawnPlayer = false;
    private float lastSpawnTime = 0;
    public static BattleManager instance;

    public CharacterFactory characterFactory;
    public AbilityManager abilityManager;

    [HideInInspector] public int currentWave = 0;
    [HideInInspector] public int enemiesLeftToSpawn;
    public bool isSpawning = false;
    public List<Character> enemyCharacters = new List<Character>();
    public List<Character> allyCharacters = new List<Character>();

    public List<CharacterBuyPanel> characterBuyPanels;
    public static float difficulty = 1;

    // ENEMY STUFF
    public int enemyDifficulty = 1;
    private float difficultyTimer = 30f;
    private float soulSpawner = 0;
    private bool roundShouldEnd = false;

    public int plinkoLevel = 1;
    private bool isBuyScreen = true;


    private int playerSpawnIndex = 0;
    private bool playerIsSpawning = false;
    private bool defeated = false;

    public enum ResourceType
    {
        POWER,
        DEFENCE,
        MAGIC
    }

    public static Dictionary<ResourceType, int> resources;

    private void Awake()
    {
        new GameEvents();
        instance = this;
        resources = new Dictionary<ResourceType, int>();
        resources.Add(ResourceType.POWER, 0);
        resources.Add(ResourceType.DEFENCE, 0);
        resources.Add(ResourceType.MAGIC, 0);
        abilityManager = new AbilityManager();
        characterFactory = new CharacterFactory(this);
        addGold(0);
    }

    private void Start()
    {
        init();
//        Time.timeScale = 10;
    }

    public void init()
    {
        tryStartNextWave();
    }

    public void tryStartNextWave()
    {
        if (isBuyScreen)
        {
            isBuyScreen = false;
            currentWave++;
            if (currentWave % 10 == 0)
            {
                enemiesLeftToSpawn = 1;
            }
            else
            {
                enemiesLeftToSpawn = 5 + currentWave / 5;
            }

            isSpawning = true;
            playerSpawnIndex = 0;
            playerIsSpawning = true;

            GameEvents.OnWaveStarted(currentWave);
        }
    }

    public bool createCharacter(bool isPlayer, CharacterStats characterStats, List<CharacterAbility> abilities, int playerCharacterId = 0)
    {
        Character closestCharacter;
        if (isPlayer)
        {
            closestCharacter = characters.Last?.Value;
            if (closestCharacter != null && closestCharacter.playerOwned)
            {
                if (closestCharacter.getPosition() >= fieldWidth - 2)
                {
                    return false;
                }
            }
        }
        else
        {
            closestCharacter = characters.First?.Value;
            if (closestCharacter != null && !closestCharacter.playerOwned)
            {
                if (closestCharacter.getPosition() <= -fieldWidth + 2)
                {
                    return false;
                }
            }
        }

        List<CharacterAbility> spawnAbilities = new List<CharacterAbility>();
        if (abilities == null || abilities.Count == 0)
        {
        }
        else
        {
            spawnAbilities.AddRange(abilities);
        }

        Character character = characterFactory.spawnCharacter(isPlayer, characterStats, spawnAbilities, playerCharacterId);
        character.render.transform.position = new Vector3(isPlayer ? fieldWidth : -fieldWidth, groundHeight, 0);
        character.render.movePosition(new Vector3(character.getPosition(), groundHeight, 0));

        if (isPlayer)
        {
            character.myNodePosition = characters.AddLast(character);
            allyCharacters.Add(character);
        }
        else
        {
            character.myNodePosition = characters.AddFirst(character);
            enemyCharacters.Add(character);
        }

        GameEvents.OnCharacterCreated(character);

        return true;
    }

    private void FixedUpdate()
    {
        if (defeated)
        {
            SceneManager.LoadScene("Menu");
            return;
        }

        if (roundShouldEnd)
        {
            var tempChar = characters.First;
            while (tempChar != null)
            {
                Character temp = tempChar.Value;
                tempChar = tempChar.Next;
                killCharacter(temp);
            }

            if (CharacterRender.ragdolls == 0)
            {
                addGold(25 + (currentWave * currentWave * 5));
                roundShouldEnd = false;
                Debug.Log("Wave Complete!");
                GameEvents.OnWaveEnded(currentWave);
                switchToBuyScreen();
            }

            return;
        }

        if (isBuyScreen)
        {
            return;
        }

        if (playerIsSpawning)
        {
            if (playerSpawnIndex < characterBuyPanels.Count)
            {
                if (createCharacter(true, characterBuyPanels[playerSpawnIndex].characterStats, characterBuyPanels[playerSpawnIndex].abilities, playerSpawnIndex))
                {
                    playerSpawnIndex++;


                    if (playerSpawnIndex >= characterBuyPanels.Count || characterBuyPanels[playerSpawnIndex].isLocked)
                    {
                        playerIsSpawning = false;
                    }
                }
            }
        }

        if (isSpawning)
        {
            if (enemiesLeftToSpawn > 0)
            {
                bool isBoss = currentWave % 10 == 0;
                float actualDifficulty = Mathf.Clamp(difficulty * currentWave * .15f, 1, difficulty);
                CharacterStats characterStats = new CharacterStats();
                characterStats.defence = (int) ((Random.Range(0, 1 + currentWave / 4) * actualDifficulty) * (isBoss ? 1.5f : 1));
                characterStats.health = (int) (Random.Range(4 + currentWave, 4 + currentWave * 2) * actualDifficulty) * (isBoss ? 6 : 1);
                characterStats.maxHealth = characterStats.health;
                characterStats.power = (int) ((Random.Range(1 + currentWave / 3, 4 + currentWave) * actualDifficulty) * (isBoss ? 1.3f : 1));
                characterStats.speed = 2;
                characterStats.turnDelay = 0;

                if (createCharacter(false, characterStats, getEnemyAbilityForWave(currentWave)))
                {
                    enemiesLeftToSpawn--;

                    if (enemiesLeftToSpawn <= 0)
                    {
                        isSpawning = false;
                    }
                }
            }
        }


        var node = characters.First;
        while (node != null)
        {
            node.Value.tick(Time.fixedDeltaTime);
            var next = node.Next;
            node = next;
        }
    }

    public List<CharacterAbility> getEnemyAbilityForWave(int waveNumber)
    {
        bool isBoss = waveNumber % 10 == 0;
        List<Ability> abilities = abilityManager.getAbilitiesForWave(waveNumber);
        List<CharacterAbility> characterAbilities = new List<CharacterAbility>();

        int number = Mathf.Clamp(Random.Range(0, 1 + waveNumber / 3), waveNumber / 2, 6);

        if (isBoss)
        {
            number = Random.Range(3, 6);
        }

        for (int i = 0; i < number; i++)
        {
            if (isBoss || Random.Range(0, 1f) > .6f + waveNumber / 20f)
            {
                int abilityLevel = Mathf.Clamp(Random.Range(1, waveNumber / 4), 1, 5);
                characterAbilities.Add(new CharacterAbility(abilities[Random.Range(0, abilities.Count)], abilityLevel));
            }
        }

        return characterAbilities;
    }

    public bool moveForward(Character character)
    {
        float targetPos = character.getForwardPosition();
        if (character.playerOwned)
        {
            if (targetPos <= -fieldWidth + 5)
            {
                return false;
            }
        }
        else if (targetPos >= fieldWidth - 5)
        {
            return false;
        }

        Character blockingChar = null;
        LinkedListNode<Character> forwardChar = character.playerOwned ? characters.Find(character).Previous : characters.Find(character).Next;
        if (forwardChar != null)
        {
            blockingChar = forwardChar.Value;
        }


        if (blockingChar != null && Mathf.Abs(blockingChar.getPosition() - character.getPosition()) < AbilityTargeter.rangeMultiplier)
        {
            return false;
        }

        character.render.movePosition(new Vector3(targetPos, groundHeight, 0));

        return true;
    }

    public void dealDamage(Character source, Character target)
    {
        bool isCrit = false;
        int damageAmount = Random.Range(source.characterStats.power / 2, source.characterStats.power);
        if (Random.Range(0, 1f) <= source.characterStats.critChance)
        {
            isCrit = true;
            damageAmount *= 2;
        }

        int damage = Mathf.Max(1, damageAmount - target.characterStats.defence);
        dealDamage(damage, target, isCrit);
    }

    public void dealDamage(int damage, Character target, bool isCrit = false)
    {
        target.characterStats.takeDamage(damage);
        GameEvents.OnCharacterHurt(target);

        DamagePopupDisplay.createPopup(Mathf.Abs(damage).ToString(), damage < 0 ? Color.green : Color.red, isCrit, target.render.transform.position + Vector3.up * 4);

        SoundManager.playGrunt();

        if (target.characterStats.health <= 0)
        {
            killCharacter(target);
        }
    }

    public void killCharacter(Character character)
    {
        if (!character.playerOwned)
        {
            addGold(5 + (enemyDifficulty * 2));
            enemyCharacters.Remove(character);
        }
        else
        {
            allyCharacters.Remove(character);
        }

        GameEvents.OnCharacterDestroyed(character);
        characters.Remove(character);
        character.destroy();

        if (!isBuyScreen)
        {
            if (!roundShouldEnd && enemyCharacters.Count == 0 && enemiesLeftToSpawn == 0)
            {
                roundShouldEnd = true;
            }

            if (!roundShouldEnd && allyCharacters.Count == 0)
            {
                defeated = true;
                Debug.Log("Player lost ;(");
            }
        }
    }

    public void switchToBuyScreen()
    {
        isBuyScreen = true;
    }

    public void addGold(int amount)
    {
        resources[ResourceType.POWER] += amount;

        GameEvents.OnGoldChanged(resources[ResourceType.POWER]);
    }

    public bool hasGold(int amount)
    {
        return resources[ResourceType.POWER] >= amount;
    }

    public bool spendGold(int amount)
    {
        if (hasGold(amount))
        {
            resources[ResourceType.POWER] -= amount;
            GameEvents.OnGoldChanged(resources[ResourceType.POWER]);
            return true;
        }

        return false;
    }

    public static int getPlinkoCost()
    {
        return 25 + (instance.plinkoLevel * instance.plinkoLevel) * 10;
    }

    public static int getPlinkoBonus(bool isBonus)
    {
        int val = (10 + (instance.plinkoLevel * instance.plinkoLevel) * 2) * (isBonus ? 2 : 1);

        if (instance.currentWave % 10 == 0)
        {
            val *= 2;
        }

        return val;
    }
}