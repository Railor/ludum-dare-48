using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Button[] startButtons;

    private void Awake()
    {
        new GameEvents();
        for (int i = 0; i < startButtons.Length; i++)
        {
            var i1 = i;
            startButtons[i].onClick.AddListener(() =>
            {
                BattleManager.difficulty = 1 + (i1 * 1.5f);
                SceneManager.LoadScene("SampleScene");
            });
        }
    }
}