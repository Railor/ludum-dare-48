using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AbilityStore : MonoBehaviour
{
    public Transform abilityContainer;
    public AbilityUI AbilityUiPrefab;

    private void Awake()
    {
        GameEvents.instance.WAVE_ENDED += GameEventsOnWaveEnded;
        GameEvents.instance.WAVE_STARTED += InstanceOnWaveStarted;
    }

    private void InstanceOnWaveStarted(int obj)
    {
    }

    private void Start()
    {
        refreshStore(1);
    }

    private void GameEventsOnWaveEnded(int waveNumber)
    {
        refreshStore(waveNumber);
    }

    public void refreshStore(int waveNumber)
    {
        List<Ability> abilities = BattleManager.instance.abilityManager.getAbilitiesForWave(waveNumber);
        List<CharacterAbility> characterAbilities = new List<CharacterAbility>();

        int number = Random.Range(3, Mathf.Clamp(waveNumber, 4, 10));
        for (int i = 0; i < number; i++)
        {
            characterAbilities.Add(new CharacterAbility(abilities[Random.Range(0, abilities.Count)], Random.Range(1, waveNumber / 2 + 1)));
        }

        abilityContainer.ClearChildren();

        for (int i = 0; i < characterAbilities.Count; i++)
        {
            AbilityUI abilityUi = Instantiate(AbilityUiPrefab, abilityContainer);
            abilityUi.characterAbility = characterAbilities[i];
            abilityUi.isDraggable = true;
            abilityUi.cost = 25 + abilityUi.characterAbility.level * (abilityUi.characterAbility.level / 2) * 25;
            abilityUi.text.text = abilityUi.characterAbility.ability.name + " " + abilityUi.characterAbility.level + " (" + abilityUi.cost + ")";
        }
    }
}