using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

public class HealAbility : Ability
{
    public float critChance;
    public float damageMod;
    public float accuracy;
    public float critChanceBonusPerLevel;
    public float damageModBonusPerLevel;

    public HealAbility()
    {
    }

    public float getDamageMod(int level)
    {
        return damageMod + ((level - 1) * damageModBonusPerLevel);
    }

    public float getCritChance(int level)
    {
        return critChance + ((level - 1) * critChanceBonusPerLevel);
    }

    public override string getDescription(CharacterAbility ability)
    {
        StringBuilder builder = new StringBuilder();
        builder.Append(base.getDescription(ability));
        builder.Append("\n\nStats");
        builder.Append("\nHeal Power%:    " + Math.Round(getDamageMod(ability.level), 2));
        builder.Append("\nAccuracy%:   " + Math.Round(accuracy, 2));
        builder.Append("\nCrit%:            " + (int) (getCritChance(ability.level) * 100));
        builder.Append("\nCooldown:     " + Math.Round(cooldown, 2));
        return builder.ToString();
    }

    public override bool canUse(float dt, Character character, BattleManager battleManager, CharacterAbility characterAbility)
    {
        List<Character> targets = abilityTargeter.getTargets(battleManager, character);

        if (targets != null && targets.Count > 0)
        {
        }
        else
        {
            return false;
        }


        return true;
    }

    public override void doAction(float dt, Character character, BattleManager battleManager, CharacterAbility characterAbility)
    {
        List<Character> targets = abilityTargeter.getTargets(battleManager, character);

        if (targets != null && targets.Count > 0)
        {
            for (int x = 0; x < targets.Count; x++)
            {
                Character target = targets[x];
                DamagePopupDisplay.createPopup(name, Color.white, false, target.render.transform.position + Vector3.up * 7);

                if (Random.Range(0, 1f) > accuracy)
                {
                    DamagePopupDisplay.createPopup("Miss", Color.yellow, false, target.render.transform.position + Vector3.up * 4);
                    // TODO MISS
                    continue;
                }

                bool isCrit = false;
                int damageAmount = (int) (Random.Range(character.characterStats.power / 2, character.characterStats.power) * getDamageMod(characterAbility.level));
                if (Random.Range(0, 1f) <= getCritChance(characterAbility.level))
                {
                    isCrit = true;
                    damageAmount *= 2;
                }

                battleManager.dealDamage(-damageAmount, target, isCrit);

                if (hitEffects != null && hitEffects.Count > 0)
                {
                    for (int i = 0; i < hitEffects.Count; i++)
                    {
                        hitEffects[i].doHit(battleManager, target);
                    }
                }
            }
        }
    }
}