using System.Collections.Generic;

public abstract class Ability
{
    public AbilityTargeter abilityTargeter;
    public List<HitEffect> hitEffects;
    public float cooldown = 3f;
    public string name = "Unnamed";
    public string description;
    public int minWaveLevel = 0;

    public abstract bool canUse(float dt, Character character, BattleManager battleManager, CharacterAbility characterAbility);
    public abstract void doAction(float dt, Character character, BattleManager battleManager, CharacterAbility characterAbility);

    public float getCooldown(CharacterAbility characterAbility)
    {
        return cooldown;
    }

    public virtual string getDescription(CharacterAbility ability)
    {
        return description;
    }
}