using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarDisplay : MonoBehaviour
{
    public Dictionary<Character, HealthBar> healthBars;
    public HealthBar healthbarPrefab;

    private void OnEnable()
    {
        healthBars = new Dictionary<Character, HealthBar>();
        GameEvents.instance.CHARACTER_CREATED += CharacterCreated;
        GameEvents.instance.CHARACTER_DESTROYED += CharacterDestroyed;
        GameEvents.instance.CHARACTER_HURT += CharacterHealthChanged;
    }

    private void OnDisable()
    {
        transform.ClearChildren();
        GameEvents.instance.CHARACTER_CREATED -= CharacterCreated;
        GameEvents.instance.CHARACTER_DESTROYED -= CharacterDestroyed;
        GameEvents.instance.CHARACTER_HURT -= CharacterHealthChanged;
    }

    private void CharacterHealthChanged(Character obj)
    {
        if (healthBars.TryGetValue(obj, out HealthBar healthbar))
        {
            healthbar.updateVisual();
        }
    }

    private void CharacterDestroyed(Character obj)
    {
        if (healthBars.TryGetValue(obj, out HealthBar healthbar))
        {
            healthBars.Remove(healthbar.character);
            Destroy(healthbar.gameObject);
        }
    }

    private void CharacterCreated(Character obj)
    {
        HealthBar healthbar = Instantiate(healthbarPrefab, transform);
        healthBars.Add(obj, healthbar);
        healthbar.setup(obj);
        healthbar.updateVisual();
    }
}