using System;
using TMPro;
using UnityEngine;

public class GoldDisplay : MonoBehaviour
{
    public TextMeshProUGUI goldText;

    private void OnEnable()
    {
        GameEvents.instance.GOLD_CHANGED += GameEventsOnGoldChanged;
    }

    private void OnDisable()
    {
        GameEvents.instance.GOLD_CHANGED -= GameEventsOnGoldChanged;
    }

    private void GameEventsOnGoldChanged(int obj)
    {
        goldText.text = "Souls: " + obj;
    }
}