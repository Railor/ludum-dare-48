using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Catcher : MonoBehaviour
{
    public BattleManager.ResourceType resourceType;
    public bool isBonus = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        BattleManager.instance.addGold(BattleManager.getPlinkoBonus(isBonus));
        DamagePopupDisplay.createPopup("+" + BattleManager.getPlinkoBonus(isBonus), Color.green, true, transform.position + Vector3.up * 4);
        Destroy(other.attachedRigidbody.gameObject);
        DamagePopupDisplay.instance.createGhost(transform.position + Vector3.up * 4);
    }
    
}