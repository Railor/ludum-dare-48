using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class CharacterRender : MonoBehaviour
{
    public static int ragdolls = 0;
    private Vector3 targetPos;
    public Animator controller;
    private Rigidbody _rigidbody;

    public enum STATE

    {
        IDLE,
        MOVING,
        ATTACKING
    }

    public STATE currentState = STATE.IDLE;
    private static readonly int IsWalking = Animator.StringToHash("IsWalking");
    private static readonly int Attack = Animator.StringToHash("Attack");

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
        controller = GetComponentInChildren<Animator>();
        controller.speed = 2;
    }

    public void movePosition(Vector3 pos)
    {
        this.targetPos = pos;
        currentState = STATE.MOVING;
    }

    public void attack()
    {
        controller.SetTrigger(Attack);
    }

    private void Update()
    {
        if (!controller.enabled)
        {
            if (transform.position.y < -40)
            {
                Destroy(gameObject);
            }

            return;
        }

        if (currentState == STATE.MOVING)
        {
            controller.SetBool(IsWalking, true);
            if (Vector3.Distance(transform.position, targetPos) < .1f)
            {
                currentState = STATE.IDLE;
                controller.SetBool(IsWalking, false);
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPos, 5f * Time.deltaTime);
            }
        }
    }

    public void ragdoll()
    {
        Destroy(gameObject, 20f);
        ragdolls++;
        controller.enabled = false;
        Rigidbody[] bodies = GetComponents<Rigidbody>();
        float minPower = 25;
        float maxPower = 45;
        foreach (Rigidbody rb in bodies)
        {
            rb.isKinematic = false;
            rb.AddForce(Random.Range(-maxPower, maxPower) * 10, Random.Range(-maxPower, maxPower) * 10, Random.Range(-maxPower, maxPower));
            rb.AddTorque(Random.Range(minPower, maxPower), Random.Range(minPower / 4, maxPower / 2), Random.Range(minPower, maxPower) * 15);
        }

        BoxCollider[] colliders = GetComponentsInChildren<BoxCollider>();
        foreach (BoxCollider rb in colliders)
        {
            rb.isTrigger = false;
        }
    }

    private void OnDestroy()
    {
        ragdolls--;
        if (ragdolls < 0)
        {
            ragdolls = 0;
        }
    }
}