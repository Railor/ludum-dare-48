using System;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;

public class ToolTipDisplay : MonoBehaviour
{
    public static ToolTipDisplay instance;
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI contentText;
    public GameObject container;

    private void Awake()
    {
        instance = this;
    }

    public static void setText(string title, string content)
    {
        instance.container.gameObject.SetActive(true);
        instance.titleText.text = title;
        instance.contentText.text = content;
    }

    public static void hide()
    {
        instance.container.gameObject.SetActive(false);
    }
}