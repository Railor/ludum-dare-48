using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plinko : MonoBehaviour
{
    public float forceMulti = 10;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision other1)
    {
        SoundManager.playPlinko();
        other1.rigidbody.AddForce((other1.transform.position - transform.position).normalized * forceMulti);
    }
}