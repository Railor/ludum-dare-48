using System;
using System.Collections.Generic;
using UnityEngine;

public class DamagePopupDisplay : MonoBehaviour
{
    public DamageText prefab;
    public static DamagePopupDisplay instance;

    private List<DamageText> available;
    private List<DamageText> inUse;

    public GameObject ghostPrefab;

    private void Awake()
    {
        instance = this;
        available = new List<DamageText>();
        inUse = new List<DamageText>();
    }

    private void Update()
    {
        for (int i = inUse.Count - 1; i >= 0; i--)
        {
            DamageText dt = inUse[i];
            dt.lifeTime -= Time.deltaTime;

            if (dt.lifeTime <= 0)
            {
                inUse.Remove(dt);
                available.Add(dt);
                dt.gameObject.SetActive(false);
            }
        }
    }

    public static void createPopup(string text, Color color, bool isBig, Vector3 position)
    {
        DamageText damageText = instance.getAvailableText();
        damageText.setup(text, color, isBig, position);
    }
    
    public static void createPopup(string text, Color color, float size, Vector3 position)
    {
        DamageText damageText = instance.getAvailableText();
        damageText.setup(text, color, size, position);
    }

    public void createGhost(Vector3 position)
    {
        GameObject ghost = Instantiate(ghostPrefab, transform);
        ghost.transform.position = position;
    }

    public DamageText getAvailableText()
    {
        if (available.Count == 0)
        {
            DamageText damageText = Instantiate(prefab, transform);
            available.Add(damageText);
        }


        DamageText first = available[0];
        first.gameObject.SetActive(true);
        available.RemoveAt(0);
        inUse.Add(first);

        return first;
    }
}