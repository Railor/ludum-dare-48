public abstract class Status
{
    public int turnsRemaining;

    public abstract void doTurn(float dt, Character character, BattleManager battleManager);
}