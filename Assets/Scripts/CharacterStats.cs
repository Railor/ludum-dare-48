public class CharacterStats
{
    public int health;
    public int maxHealth;
    public int power;
    public int defence;
    public float speed;
    public float turnDelay;
    public float critChance;

    public CharacterStats()
    {
    }

    public CharacterStats(CharacterStats stats)
    {
        this.health = stats.health;
        this.maxHealth = this.health;
        this.power = stats.power;
        this.defence = stats.defence;
        this.speed = stats.speed;
        this.critChance = stats.critChance;
    }

    public void takeDamage(int damage)
    {
        health -= damage;

        if (health < 0)
        {
            health = 0;
        }

        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    public bool isHurt()
    {
        return health < maxHealth;
    }
}