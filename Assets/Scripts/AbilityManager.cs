using System.Collections.Generic;

public class AbilityManager
{
    public List<Ability> abilties;
    public static Ability meleeAbility;

    public AbilityManager()
    {
        abilties = new List<Ability>();
        generateAbilities();
    }

    private void generateAbilities()
    {
        AttackAbility melee = new AttackAbility();
        melee.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.ENEMY);
        melee.abilityTargeter.maxRange = 1;
        melee.abilityTargeter.minRange = 0;
        melee.cooldown = 1f;
        melee.accuracy = .9f;
        melee.critChance = .1f;
        melee.damageMod = 1f;
        melee.critChanceBonusPerLevel = .05f;
        melee.damageModBonusPerLevel = .1f;
        melee.name = "Melee";
        melee.description = "Basic attack";
        meleeAbility = melee;

        AttackAbility poke = new AttackAbility();
        poke.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.ENEMY);
        poke.abilityTargeter.maxRange = 3;
        poke.abilityTargeter.maxTargets = 1;
        poke.cooldown = 6f;
        poke.accuracy = .9f;
        poke.critChance = .1f;
        poke.damageMod = 1f;
        poke.minWaveLevel = 5;
        poke.critChanceBonusPerLevel = .05f;
        poke.damageModBonusPerLevel = .1f;
        poke.name = "Poke";
        poke.description = "Poke a target up to 3 characters ahead";
        abilties.Add(poke);

        HealAbility healAbility = new HealAbility();
        healAbility.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.SELF, AbilityTargeter.TARGET_TYPE.TEAM);
        healAbility.abilityTargeter.maxRange = 1;
        healAbility.abilityTargeter.minRange = 0;
        healAbility.cooldown = 8f;
        healAbility.accuracy = 1;
        healAbility.critChance = .1f;
        healAbility.damageMod = .5f;
        healAbility.critChanceBonusPerLevel = .1f;
        healAbility.damageModBonusPerLevel = .2f;
        healAbility.name = "Heal Self";
        healAbility.description = "Heal yourself";
        abilties.Add(healAbility);

        HealAbility healingHands = new HealAbility();
        healingHands.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.TEAM);
        healingHands.abilityTargeter.maxRange = 1;
        healingHands.abilityTargeter.minRange = 0;
        healingHands.cooldown = 10f;
        healingHands.accuracy = 1;
        healingHands.critChance = .1f;
        healingHands.damageMod = .5f;
        healingHands.critChanceBonusPerLevel = .1f;
        healingHands.damageModBonusPerLevel = .2f;
        healingHands.name = "Healing Hands";
        healingHands.description = "Heal 1 ally infront";
        abilties.Add(healingHands);

        HealAbility restoration = new HealAbility();
        restoration.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.TEAM);
        restoration.abilityTargeter.maxRange = 3;
        restoration.abilityTargeter.minRange = 0;
        restoration.abilityTargeter.maxTargets = 3;
        restoration.cooldown = 13f;
        restoration.accuracy = 1;
        restoration.critChance = .1f;
        restoration.damageMod = .3f;
        restoration.minWaveLevel = 3;
        restoration.critChanceBonusPerLevel = .1f;
        restoration.damageModBonusPerLevel = .2f;
        restoration.name = "Heal Other";
        restoration.description = "Heal 3 allies infront";
        abilties.Add(restoration);

        AttackAbility cleave = new AttackAbility();
        cleave.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.ENEMY);
        cleave.abilityTargeter.maxRange = 2;
        cleave.abilityTargeter.minRange = 0;
        cleave.abilityTargeter.maxTargets = 2;
        cleave.cooldown = 6f;
        cleave.accuracy = .9f;
        cleave.critChance = .1f;
        cleave.damageMod = .5f;
        cleave.critChanceBonusPerLevel = .05f;
        cleave.damageModBonusPerLevel = .05f;
        cleave.name = "Cleave";
        cleave.description = "Basic attack that hits 2 enemies in front";
        abilties.Add(cleave);

        AttackAbility deathStrike = new AttackAbility();
        deathStrike.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.ENEMY);
        deathStrike.abilityTargeter.maxRange = 1;
        deathStrike.abilityTargeter.minRange = 0;
        deathStrike.cooldown = 10f;
        deathStrike.accuracy = .6f;
        deathStrike.critChance = .1f;
        deathStrike.damageMod = 2.5f;
        deathStrike.minWaveLevel = 15;
        deathStrike.critChanceBonusPerLevel = .05f;
        deathStrike.damageModBonusPerLevel = .25f;
        deathStrike.name = "DeathStrike";
        deathStrike.description = "Slow heavy hitting attack";

        abilties.Add(deathStrike);

        AttackAbility slashAbility = new AttackAbility();
        slashAbility.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.ENEMY);
        slashAbility.abilityTargeter.maxRange = 2;
        slashAbility.abilityTargeter.minRange = 0;
        slashAbility.abilityTargeter.maxTargets = 2;
        slashAbility.cooldown = 7f;
        slashAbility.accuracy = .6f;
        slashAbility.critChance = .1f;
        slashAbility.damageMod = 1f;
        slashAbility.critChanceBonusPerLevel = .05f;
        slashAbility.damageModBonusPerLevel = .1f;
        slashAbility.name = "Slash";
        slashAbility.minWaveLevel = 6;
        slashAbility.description = "Slashes infront hitting 3 enemies";
        abilties.Add(slashAbility);

        AttackAbility cutAbility = new AttackAbility();
        cutAbility.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.ENEMY);
        cutAbility.abilityTargeter.maxRange = 1;
        cutAbility.abilityTargeter.minRange = 0;
        cutAbility.cooldown = 7f;
        cutAbility.accuracy = .9f;
        cutAbility.critChance = .1f;
        cutAbility.damageMod = 1.5f;
        cutAbility.critChanceBonusPerLevel = .1f;
        cutAbility.damageModBonusPerLevel = .2f;
        cutAbility.name = "Cut";
        cutAbility.description = "Deeply cut the target";
        abilties.Add(cutAbility);

        AttackAbility sweep = new AttackAbility();
        sweep.abilityTargeter = new AbilityTargeter(AbilityTargeter.TARGET_POSITIONS.FORWARD, AbilityTargeter.TARGET_TYPE.ENEMY);
        sweep.abilityTargeter.maxRange = 5;
        sweep.abilityTargeter.minRange = 0;
        sweep.abilityTargeter.maxTargets = 5;
        sweep.cooldown = 7f;
        sweep.accuracy = .8f;
        sweep.critChance = .1f;
        sweep.damageMod = .25f;
        sweep.critChanceBonusPerLevel = .05f;
        sweep.damageModBonusPerLevel = .05f;
        sweep.name = "Sweep";
        sweep.minWaveLevel = 7;
        sweep.description = "Basic attack that hits up to 5 enemies ahead";
        abilties.Add(sweep);
    }

    public List<Ability> getAbilitiesForWave(int waveNumber)
    {
        List<Ability> newAbilities = new List<Ability>();
        for (int i = 0; i < abilties.Count; i++)
        {
            if (abilties[i].minWaveLevel <= waveNumber)
            {
                newAbilities.Add(abilties[i]);
            }
        }

        return newAbilities;
    }
}