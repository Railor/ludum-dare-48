using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] gruntSounds;
    public AudioClip[] plinkoSounds;
    public AudioSource plinkoSource;
    public AudioSource gruntSource;
    public static SoundManager instance;

    public Slider volumeSlider;
    private float volumeScale = 1f;

    private void Awake()
    {
        instance = this;
        volumeSlider.onValueChanged.AddListener(volumeSliderChanged);
    }

    private void volumeSliderChanged(float arg0)
    {
        volumeScale = arg0;
    }


    public static void playPlinko()
    {
        instance.plinkoSource.PlayOneShot(instance.plinkoSounds[Random.Range(0, instance.plinkoSounds.Length)], .1f * instance.volumeScale);
    }

    public static void playGrunt()
    {
        instance.gruntSource.PlayOneShot(instance.gruntSounds[Random.Range(0, instance.gruntSounds.Length)], .7f * instance.volumeScale);
    }
}