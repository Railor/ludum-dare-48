using UnityEngine;

public class CharacterAbility
{
    public int level;
    public Ability ability;
    public float cooldown;

    public CharacterAbility(Ability ability, int level)
    {
        this.ability = ability;
        this.level = level;
    }

    public bool canUse(float dt, Character character, BattleManager battleManager)
    {
        return cooldown <= 0 && ability.canUse(dt, character, battleManager, this);
    }

    public void doAction(float dt, Character character, BattleManager battleManager)
    {
        ability.doAction(dt, character, battleManager, this);
        cooldown = ability.cooldown;
    }

    public string getTitle()
    {
        return ability.name + " " + level;
    }

    public string getDescription()
    {
        return ability.getDescription(this);
    }

    public int getUpgradeCost()
    {
        int tempLevel = Mathf.Max(level - 2, 0);
        return 25 + tempLevel * (tempLevel/2) * 25;
    }
}