using System;

public class GameEvents
{
    public static GameEvents instance;
    public event Action<int> GOLD_CHANGED;
    public event Action<int> WAVE_ENDED;
    public event Action<int> WAVE_STARTED;

    public event Action<Character> CHARACTER_CREATED;
    public event Action<Character> CHARACTER_DESTROYED;
    public event Action<Character> CHARACTER_HURT;

    public GameEvents()
    {
        instance = this;
    }

    public static void OnGoldChanged(int obj)
    {
        instance.GOLD_CHANGED?.Invoke(obj);
    }

    public static void OnWaveEnded(int obj)
    {
        instance.WAVE_ENDED?.Invoke(obj);
    }

    public static void OnWaveStarted(int obj)
    {
        instance.WAVE_STARTED?.Invoke(obj);
    }

    public static void OnCharacterCreated(Character obj)
    {
        instance.CHARACTER_CREATED?.Invoke(obj);
    }

    public static void OnCharacterDestroyed(Character obj)
    {
        instance.CHARACTER_DESTROYED?.Invoke(obj);
    }

    public static void OnCharacterHurt(Character obj)
    {
        instance.CHARACTER_HURT?.Invoke(obj);
    }
}