using System.Collections.Generic;
using UnityEngine;

public class HitEffect
{
    public float chance;
    public List<Status> applyStatuses;

    public void doHit(BattleManager battleManager, Character target)
    {
        if (Random.Range(0, 1f) >= chance)
        {
            return;
        }

        if (applyStatuses != null && applyStatuses.Count > 0)
        {
            for (int i = 0; i < applyStatuses.Count; i++)
            {
            }
        }
    }
}