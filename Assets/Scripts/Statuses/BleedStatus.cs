using UnityEngine;

public class BleedStatus : Status
{
    public int damage;

    public override void doTurn(float dt, Character character, BattleManager battleManager)
    {
        battleManager.dealDamage(damage, character);
    }
}