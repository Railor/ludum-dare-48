using System.Collections.Generic;
using UnityEngine;

public class AbilityTargeter
{
    public int maxRange;
    public int minRange;
    public int maxTargets = 1;
    public TARGET_POSITIONS targetPositions;
    public TARGET_TYPE targetType;
    public bool lessThanMaxHealth = false;
    public static float rangeMultiplier = 4;


    public AbilityTargeter(TARGET_POSITIONS targetPositions, TARGET_TYPE targetType)
    {
        this.targetPositions = targetPositions;
        this.targetType = targetType;
    }

    public List<Character> getTargets(BattleManager battleManager, Character sourceCharacter)
    {
        List<Character> characters = new List<Character>();
        if (targetPositions == TARGET_POSITIONS.SELF)
        {
            if (!lessThanMaxHealth || sourceCharacter.characterStats.isHurt())
            {
                characters.Add(sourceCharacter);
            }

            return characters;
        }

        if (targetPositions == TARGET_POSITIONS.FORWARD)
        {
            Character character = sourceCharacter.getInfrontCharacter(sourceCharacter.playerOwned);
            while (character != null && characters.Count < maxTargets)
            {
                float dst = Mathf.Abs(character.getPosition() - sourceCharacter.getPosition());
                if (dst > maxRange * rangeMultiplier)
                {
                    break;
                }
                else if (dst < minRange * rangeMultiplier)
                {
                    character = character.getInfrontCharacter(sourceCharacter.playerOwned);
                    continue;
                }

                if (targetType == TARGET_TYPE.BOTH)
                {
                    if (!lessThanMaxHealth || character.characterStats.isHurt())
                    {
                        characters.Add(character);
                    }
                }
                else if (targetType == TARGET_TYPE.TEAM)
                {
                    if (character.playerOwned == sourceCharacter.playerOwned)
                    {
                        if (!lessThanMaxHealth || character.characterStats.isHurt())
                        {
                            characters.Add(character);
                        }
                    }
                }
                else if (character.playerOwned != sourceCharacter.playerOwned)
                {
                    if (!lessThanMaxHealth || character.characterStats.isHurt())
                    {
                        characters.Add(character);
                    }
                }

                character = character.getInfrontCharacter(sourceCharacter.playerOwned);
            }
        }

        return characters;
    }


    public enum TARGET_POSITIONS
    {
        SELF,
        FORWARD,
    }

    public enum TARGET_TYPE
    {
        TEAM,
        ENEMY,
        BOTH
    }
}