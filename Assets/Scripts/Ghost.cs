using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ghost : MonoBehaviour
{
    private void Awake()
    {
        Destroy(gameObject, 2f);
    }

    private void Update()
    {
        transform.position = new Vector3(transform.position.x + Random.Range(-1 * Time.deltaTime, 1 * Time.deltaTime), transform.position.y + Time.deltaTime * 5, transform.position.z);
        transform.localScale = transform.localScale * (1 - Time.deltaTime * .9f);
    }
}