using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePanel : MonoBehaviour
{
    public GameObject container;
    public Button plinkoBuyButton;
    public TextMeshProUGUI plinkoLevelText;
    public TextMeshProUGUI plinkoBuyText;
    public Button startNextWave;

    private void Awake()
    {
        plinkoBuyButton.onClick.AddListener(plinkClicked);
        startNextWave.onClick.AddListener(StartNextWave);

        GameEvents.instance.WAVE_STARTED += GameEventsOnWaveStarted;
        GameEvents.instance.WAVE_ENDED += GameEventsOnWaveEnded;
        updateText();
    }

    private void GameEventsOnWaveEnded(int obj)
    {
        container.gameObject.SetActive(true);
    }

    private void GameEventsOnWaveStarted(int obj)
    {
        container.gameObject.SetActive(false);
    }

    private void StartNextWave()
    {
        BattleManager.instance.tryStartNextWave();
    }

    private void plinkClicked()
    {
        if (BattleManager.instance.hasGold(BattleManager.getPlinkoCost()))
        {
            BattleManager.instance.spendGold(BattleManager.getPlinkoCost());
            BattleManager.instance.plinkoLevel++;
            updateText();
        }
        else
        {
            DamagePopupDisplay.createPopup("Cant afford", Color.red, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }

    public void updateText()
    {
        plinkoLevelText.text = "Soul Bonus: " + (BattleManager.getPlinkoBonus(false));
        plinkoBuyText.text = BattleManager.getPlinkoCost().ToString();
    }
}