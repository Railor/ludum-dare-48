using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [HideInInspector] public Character character;
    public RectTransform foreground;
    public Image background;
    public TextMeshProUGUI text;

    private float positionOffset = -.5f;
    private Camera gameCamera;
    private float width;

    private void Awake()
    {
        gameCamera = Camera.main;
        width = foreground.sizeDelta.x;
    }

    public void setup(Character character)
    {
        this.character = character;
        updateVisual();
    }


    public void updateVisual()
    {
        // Do stuff
        float fillPercent = (character.characterStats.health / (float) character.characterStats.maxHealth);
        foreground.sizeDelta = new Vector2(fillPercent * width, foreground.sizeDelta.y);
        this.text.text = character.characterStats.health + " / " + character.characterStats.maxHealth;
    }

    private void LateUpdate()
    {
        if (!character.isDead)
        {
            transform.position = gameCamera.WorldToScreenPoint(character.render.transform.position + Vector3.up * positionOffset);
        }
    }
}