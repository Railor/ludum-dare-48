using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CharacterBuyPanel : MonoBehaviour, IDropHandler
{
    public static AbilityUI draggedAbility;

    public Button levelButton;
    public TextMeshProUGUI levelCostAmountText;
    public TextMeshProUGUI levelNumberText;
    public TextMeshProUGUI purchaseText;

    public Transform lockedOverlayTransform;
    public Transform charOverlayTransform;
    public Button unlockButton;

    public Transform abilityContainer;
    public AbilityUI AbilityUiPrefab;

    public TextMeshProUGUI statsText;
    public CharacterStats characterStats;
    public List<CharacterAbility> abilities = new List<CharacterAbility>();
    public int level = 0;

    private int spawnCost = 6;
    private int levelCost = 25;
    public bool isLockedAtStart;
    public int unlockCost;
    public int startingLevel = 1;
    [HideInInspector] public bool isLocked;

    private void Awake()
    {
        isLocked = isLockedAtStart;
        characterStats = new CharacterStats();
        characterStats.defence = 1;
        characterStats.health = 25;
        characterStats.maxHealth = characterStats.health;
        characterStats.power = 5;
        characterStats.speed = 1;
        characterStats.turnDelay = 0;
        characterStats.critChance = .1f;

        if (isLockedAtStart)
        {
            purchaseText.text = "Purchase: " + unlockCost;
            lockedOverlayTransform.gameObject.SetActive(true);
            unlockButton.onClick.AddListener(() =>
            {
                if (BattleManager.instance.hasGold(unlockCost))
                {
                    BattleManager.instance.spendGold(unlockCost);
                    charOverlayTransform.gameObject.SetActive(true);
                    lockedOverlayTransform.gameObject.SetActive(false);
                    isLocked = false;
                }
                else
                {
                    DamagePopupDisplay.createPopup("Cant afford", Color.red, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                }
            });
        }
        else
        {
            charOverlayTransform.gameObject.SetActive(true);
            lockedOverlayTransform.gameObject.SetActive(false);
        }

        buildStatsText();
    }

    public void generateStatsForLevel()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        levelButton.onClick.AddListener(LevelButtonPressed);
        buildAbilities();

        levelCostAmountText.text = "Level Up: " + levelCost;
    }

    private void LevelButtonPressed()
    {
        if (BattleManager.instance.hasGold(levelCost))
        {
            BattleManager.instance.spendGold(levelCost);
            level++;
            levelCost += (int) (levelCost * .5f);
            levelCostAmountText.text = "Level Up: " + levelCost;
            levelNumberText.text = "Level: " + level;
            characterStats.health += Random.Range(level, level  * 3);
            characterStats.maxHealth = characterStats.health;
            characterStats.defence += Random.Range(0, 2 + level / 2);
            characterStats.power += Random.Range(1 + (level / 3), 2 + (level / 2));
            characterStats.critChance += Random.Range(.01f, .05f);
            spawnCost = levelCost / 4;
            buildStatsText();
        }
        else
        {
            DamagePopupDisplay.createPopup("Cant afford", Color.red, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }

    private void buildStatsText()
    {
        string content = "Stats\n";
        content += "Power: " + characterStats.power + "\n";
        content += "Defence: " + characterStats.defence + "\n";
        content += "Health: " + characterStats.maxHealth + "\n";
        content += "Crit %: " + (int) (characterStats.critChance * 100) + "\n";
        statsText.text = content;
    }

    public void buildAbilities()
    {
        abilityContainer.ClearChildren();

        for (int i = 0; i < abilities.Count; i++)
        {
            AbilityUI abilityUi = Instantiate(AbilityUiPrefab, abilityContainer);
            abilityUi.characterAbility = abilities[i];
            abilityUi.text.text = abilityUi.characterAbility.ability.name + " " + abilityUi.characterAbility.level;
            abilityUi.characterBuyPanel = this;
        }
    }

    public void upgradeAbility(AbilityUI abilityUi)
    {
        if (BattleManager.instance.hasGold(abilityUi.characterAbility.getUpgradeCost()))
        {
            BattleManager.instance.spendGold(abilityUi.characterAbility.getUpgradeCost());
            abilityUi.characterAbility.level += 1;
            buildAbilities();
        }
        else
        {
            DamagePopupDisplay.createPopup("Cant afford", Color.red, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }

    public void sellAbility(AbilityUI abilityUi)
    {
        DamagePopupDisplay.createPopup("Sold: " + abilityUi.characterAbility.getUpgradeCost(), Color.green, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        BattleManager.instance.addGold(abilityUi.characterAbility.getUpgradeCost());
        abilities.Remove(abilityUi.characterAbility);
        buildAbilities();
        ToolTipDisplay.hide();
    }


    // Update is called once per frame
    void Update()
    {
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!draggedAbility)
        {
            return;
        }

        for (int i = 0; i < abilities.Count; i++)
        {
            if (abilities[i].ability == draggedAbility.characterAbility.ability)
            {
                DamagePopupDisplay.createPopup("Already have!", Color.red, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
                return;
            }
        }

        if (abilities.Count >= 6)
        {
            DamagePopupDisplay.createPopup("Max Abilities", Color.red, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        if (BattleManager.instance.hasGold(draggedAbility.cost))
        {
            BattleManager.instance.spendGold(draggedAbility.cost);
            abilities.Add(new CharacterAbility(draggedAbility.characterAbility.ability, draggedAbility.characterAbility.level));
            buildAbilities();
        }
        else
        {
            DamagePopupDisplay.createPopup("Cant afford", Color.red, 15f, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}