using System.Collections.Generic;

public class CharacterFactory
{
    private BattleManager battleManager;

    public CharacterFactory(BattleManager battleManager)
    {
        this.battleManager = battleManager;
    }

    public Character spawnCharacter(bool playerOwned, CharacterStats characterStats, List<CharacterAbility> abilties, int playerCharacterId)
    {
        Character character = new Character(playerOwned, battleManager, playerCharacterId);
        character.characterStats = new CharacterStats(characterStats);

        for (int i = 0; i < abilties.Count; i++)
        {
            character.abilities.Add(new CharacterAbility(abilties[i].ability, abilties[i].level));
        }

        return character;
    }
}