using System;
using TMPro;
using UnityEngine;

public class DamageText : MonoBehaviour
{
    public TextMeshProUGUI text;
    [HideInInspector] public float lifeTime;

    private Camera camera;

    private void Awake()
    {
        camera = Camera.main;
    }

    private void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * 40, transform.position.z);
        transform.localScale = transform.localScale * (1 - Time.deltaTime * .9f);
    }

    public void setup(string content, Color color, bool isBig, Vector3 position)
    {
        text.text = content;
        text.color = color;
        text.fontSize = isBig ? 35 : 25;
        lifeTime = 1.5f;
        transform.position = camera.WorldToScreenPoint(position);
        transform.localScale = Vector3.one;
    }
    
    public void setup(string content, Color color, float fontSize, Vector3 position)
    {
        text.text = content;
        text.color = color;
        text.fontSize = fontSize;
        lifeTime = 1.5f;
        transform.position = camera.WorldToScreenPoint(position);
        transform.localScale = Vector3.one;
    }
}